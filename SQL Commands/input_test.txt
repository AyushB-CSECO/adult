# Create schema of input test table
/*CREATE TABLE IF NOT EXISTS prj_adult.`input_test` (
  `age` int(11) DEFAULT NULL,
  `workclass` varchar(50) DEFAULT NULL,
  `fnlwght` float DEFAULT NULL,
  `education` varchar(50) DEFAULT NULL,
  `education_num` int(11) DEFAULT NULL,
  `maritial_status` varchar(50) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  `race` varchar(50) DEFAULT NULL,
  `sex` varchar(50) DEFAULT NULL,
  `capital_gain` float DEFAULT NULL,
  `capital_loss` float DEFAULT NULL,
  `hours_per_week` int(11) DEFAULT NULL,
  `native_country` varchar(50) DEFAULT NULL,
  `income` varchar(50) DEFAULT NULL
) */

------------------------------------------------------------------------------------------------------------------------

# Alter the response column income("<=50K." --> L, ">50K." --> R )

/* ALTER TABLE prj_adult.input_test
   ADD income_class VARCHARACTER(10)

   UPDATE prj_adult.input_test
   SET income_class = 'L' WHERE income = ' <=50K.'

   UPDATE prj_adult.input_test
   SET income_class = 'H' WHERE income = ' >50K.'

   ALTER TABLE prj_adult.input_test DROP `income` */

------------------------------------------------------------------------------------------------------------------------

